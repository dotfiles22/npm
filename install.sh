#!/bin/bash

# Définition des fichiers liés à npm
NPM_CONFIG_FILE=$HOME/.config/npm/npmrc
NPM_GLOBAL_CONFIG_FILE=$HOME/.config/npm/global-npmrc
NPM_LOCAL_CONFIG_FILE=$HOME/.config/npm/local-npmrc

# Construction du npmrc
touch $NPM_LOCAL_CONFIG_FILE
echo "# Global config" > $NPM_CONFIG_FILE
cat $NPM_GLOBAL_CONFIG_FILE >> $NPM_CONFIG_FILE
echo "# Local config" >> $NPM_CONFIG_FILE
cat $NPM_LOCAL_CONFIG_FILE >> $NPM_CONFIG_FILE

# Mise à jour du npmrc
unlink ~/.npmrc
ln -s ~/.config/npm/npmrc ~/.npmrc